# Studio: quick edit for your videos <Badge type="info" text="PeerTube >= 4.2"></Badge>

After uploading a video, you have the option to briefly edit it using **Studio**. You can:

* cut the video with a new start and/or end
* Add an intro video (credits, for example)
* Add an end video (e.g. credits)
* Add a watermark/logo (in `png`, `jpg`, `jpeg` or `webp` format) to the video.

To do this, **once the video has been published**, you will need to

1. click on **…** under your video
1. click on **Studio**
  ![image studio interface](/assets/EN_studio_vue_video.png)
1. make your changes (the list of changes will appear below the video thumbnail)
1. click **Run video edition**.

![image video edition page](/assets/EN_studio_edition.png)
