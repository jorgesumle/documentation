#!/bin/sh

set -eu

rm -rf ./docs/remote-markdown ./docs/remote-code

curl -s https://framagit.org/framasoft/peertube/PeerTube/-/raw/develop/CREDITS.md --create-dirs -o ./docs/remote-markdown/CREDITS.md
curl -s https://framagit.org/framasoft/peertube/PeerTube/-/raw/develop/CHANGELOG.md --create-dirs -o ./docs/remote-markdown/CHANGELOG.md
curl -s https://framagit.org/framasoft/peertube/PeerTube/-/raw/develop/CODE_OF_CONDUCT.md --create-dirs -o ./docs/remote-markdown/CODE_OF_CONDUCT.md

curl -s https://framagit.org/framasoft/peertube/PeerTube/-/raw/develop/.github/CONTRIBUTING.md --create-dirs -o ./docs/remote-markdown/CONTRIBUTING.md

for i in dependencies.md \
  production.md \
  translation.md \
  docker.md \
  tools.md \
  plugins/guide.md \
  api/quickstart.md \
  api/embeds.md \
  development/ci.md \
  development/lib.md \
  development/localization.md \
  development/monitoring.md \
  development/release.md \
  development/server.md \
  development/tests.md;
do
  curl -s "https://framagit.org/framasoft/peertube/PeerTube/-/raw/develop/support/doc/$i" --create-dirs -o "./docs/remote-markdown/doc/$i"
done

for i in shared/models/plugins/server/server-hook.model.ts \
  client/src/types/client-script.model.ts \
  client/src/types/register-client-option.model.ts \
  server/types/plugins/plugin-library.model.ts \
  server/types/plugins/register-server-auth.model.ts \
  server/types/plugins/register-server-option.model.ts \
  shared/models/activitypub/objects/cache-file-object.ts \
  shared/models/activitypub/objects/playlist-element-object.ts \
  shared/models/activitypub/objects/playlist-object.ts \
  shared/models/activitypub/objects/video-comment-object.ts \
  shared/models/activitypub/objects/video-torrent-object.ts \
  shared/models/plugins/client/client-hook.model.ts \
  shared/models/plugins/client/plugin-client-scope.type.ts \
  shared/models/plugins/client/plugin-element-placeholder.type.ts \
  shared/models/plugins/client/plugin-selector-id.type.ts \
  shared/models/plugins/client/register-client-form-field.model.ts \
  shared/models/plugins/client/register-client-form-field.model.ts \
  shared/models/plugins/client/register-client-hook.model.ts \
  shared/models/plugins/client/register-client-settings-script.model.ts \
  shared/models/plugins/server/managers/plugin-playlist-privacy-manager.model.ts \
  shared/models/plugins/server/managers/plugin-settings-manager.model.ts \
  shared/models/plugins/server/managers/plugin-storage-manager.model.ts \
  shared/models/plugins/server/managers/plugin-transcoding-manager.model.ts \
  shared/models/plugins/server/managers/plugin-video-category-manager.model.ts \
  shared/models/plugins/server/managers/plugin-video-language-manager.model.ts \
  shared/models/plugins/server/managers/plugin-video-licence-manager.model.ts \
  shared/models/plugins/server/managers/plugin-video-privacy-manager.model.ts \
  shared/models/plugins/server/register-server-hook.model.ts \
  shared/models/plugins/server/settings/register-server-setting.model.ts \
  shared/models/activitypub/objects/watch-action-object.ts \
  shared/models/videos/transcoding/video-transcoding.model.ts;
do
  curl -s "https://framagit.org/framasoft/peertube/PeerTube/-/raw/develop/$i" --create-dirs -o "./docs/remote-code/$i"
done
