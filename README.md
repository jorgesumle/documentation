# PeerTube documentation

## Dev

```
$ npm ci
$ npm run update-remote-files
$ npm run build-rest-api-doc
$ npm run dev
```

## Build

```
$ npm ci
$ npm run build
$ npm run preview
```
